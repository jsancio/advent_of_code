use anyhow::anyhow;
use anyhow::Result;
use core::cell::RefCell;
use nom::branch::alt;
use nom::bytes::complete::tag;
use nom::character::complete::digit1;
use nom::combinator::map;
use nom::combinator::map_res;
use nom::combinator::opt;
use nom::error::make_error;
use nom::error::ErrorKind;
use nom::IResult;
use std::collections::VecDeque;
use std::convert::TryFrom;
use std::convert::TryInto;
use std::io::Read;
use std::rc::Rc;
use std::str::FromStr;

#[derive(Debug)]
enum Value {
    Number(i64),
    Register(u8),
}

#[derive(Debug)]
enum Operation {
    Add(u8, Value),
    Mod(u8, Value),
    Mul(u8, Value),
    Set(u8, Value),
    Jgz(Value, Value),
    Rcv(u8),
    Snd(u8),
}

impl Operation {
    /// Executes the command in a context.
    ///
    /// Returns `false` if the operation was a rcv operation and it would have blocked.
    fn execute(&self, context: &mut Context) -> bool {
        let mut skip = match self {
            Operation::Add(register, value) => {
                *context.register_mut(*register) += context.number(value);
                1
            }
            Operation::Mod(register, value) => {
                *context.register_mut(*register) %= context.number(value);
                1
            }
            Operation::Mul(register, value) => {
                *context.register_mut(*register) *= context.number(value);
                1
            }
            Operation::Set(register, value) => {
                *context.register_mut(*register) = context.number(value);
                1
            }
            Operation::Jgz(first, second) => {
                if context.number(first) > 0 {
                    context.number(second)
                } else {
                    1
                }
            }
            Operation::Rcv(register) => {
                if let Some(number) = context.rcv() {
                    *context.register_mut(*register) = number;
                    1
                } else {
                    return false;
                }
            }
            Operation::Snd(register) => {
                context.snd(context.register(*register));
                1
            }
        };

        if skip > 0 {
            context.instruction_counter += usize::try_from(skip).unwrap();
        } else {
            skip *= -1;
            context.instruction_counter -= usize::try_from(skip).unwrap();
        }

        true
    }
}

#[derive(Debug)]
struct Context {
    is_zero: bool,
    registers: Vec<i64>,
    instruction_counter: usize,
    snd: Rc<RefCell<VecDeque<i64>>>,
    rcv: Rc<RefCell<VecDeque<i64>>>,
    snd_counter: usize,
}

impl Context {
    fn new() -> (Self, Self) {
        let zero_queue = Rc::new(RefCell::new(VecDeque::new()));
        let one_queue = Rc::new(RefCell::new(VecDeque::new()));

        let zero = Self {
            is_zero: true,
            registers: vec![0; 16],
            instruction_counter: 0,
            snd: one_queue.clone(),
            rcv: zero_queue.clone(),
            snd_counter: 0,
        };

        let one = {
            const P: usize = 15;
            let mut registers = vec![0; 16];
            registers[P] = 1;

            Self {
                is_zero: false,
                registers,
                instruction_counter: 0,
                snd: zero_queue,
                rcv: one_queue,
                snd_counter: 0,
            }
        };

        (zero, one)
    }

    fn number(&self, value: &Value) -> i64 {
        match value {
            &Value::Number(number) => number,
            &Value::Register(register) => self.register(register),
        }
    }

    fn register_mut(&mut self, register: u8) -> &mut i64 {
        &mut self.registers[usize::from(register)]
    }

    fn register(&self, register: u8) -> i64 {
        self.registers[usize::from(register)]
    }

    fn rcv(&mut self) -> Option<i64> {
        self.rcv.borrow_mut().pop_front()
    }

    fn snd(&mut self, value: i64) {
        self.snd.borrow_mut().push_back(value);
        self.snd_counter += 1;
    }
}

fn number(input: &str) -> IResult<&str, i64> {
    let (input, sign) = opt(tag("-"))(input)?;
    let (input, number) = map_res(digit1, i64::from_str)(input)?;

    let sign = sign.map_or(1, |_| -1);

    Ok((input, sign * number))
}

fn register(input: &str) -> IResult<&str, u8> {
    if let Some(c) = input.chars().next() {
        if c.is_ascii_alphabetic() {
            let input = &input[1..];
            // SAFETY: unwrap is safe because is_ascii_alphabetic is true
            let mut register = u32::from(c).try_into().unwrap();
            register -= b'a';
            Ok((input, register))
        } else {
            Err(nom::Err::Error(make_error(input, ErrorKind::Char)))
        }
    } else {
        Err(nom::Err::Error(make_error(input, ErrorKind::Eof)))
    }
}

fn value(input: &str) -> IResult<&str, Value> {
    let number_value = map(number, |n| Value::Number(n));
    let register_value = map(register, |r| Value::Register(r));

    alt((number_value, register_value))(input)
}

fn resgister_and_value(input: &str) -> IResult<&str, (u8, Value)> {
    let (input, register) = register(input)?;
    let (input, _) = tag(" ")(input)?;
    let (input, value) = value(input)?;

    Ok((input, (register, value)))
}

fn add(input: &str) -> IResult<&str, Operation> {
    let (input, _) = tag("add ")(input)?;
    let (input, (register, value)) = resgister_and_value(input)?;

    Ok((input, Operation::Add(register, value)))
}

fn jgz(input: &str) -> IResult<&str, Operation> {
    let (input, _) = tag("jgz ")(input)?;
    let (input, first) = value(input)?;
    let (input, _) = tag(" ")(input)?;
    let (input, second) = value(input)?;

    Ok((input, Operation::Jgz(first, second)))
}

fn modulo(input: &str) -> IResult<&str, Operation> {
    let (input, _) = tag("mod ")(input)?;
    let (input, (register, value)) = resgister_and_value(input)?;

    Ok((input, Operation::Mod(register, value)))
}

fn mul(input: &str) -> IResult<&str, Operation> {
    let (input, _) = tag("mul ")(input)?;
    let (input, (register, value)) = resgister_and_value(input)?;

    Ok((input, Operation::Mul(register, value)))
}

fn set(input: &str) -> IResult<&str, Operation> {
    let (input, _) = tag("set ")(input)?;
    let (input, (register, value)) = resgister_and_value(input)?;

    Ok((input, Operation::Set(register, value)))
}

fn rcv(input: &str) -> IResult<&str, Operation> {
    let (input, _) = tag("rcv ")(input)?;
    let (input, register) = register(input)?;

    Ok((input, Operation::Rcv(register)))
}

fn snd(input: &str) -> IResult<&str, Operation> {
    let (input, _) = tag("snd ")(input)?;
    let (input, register) = register(input)?;

    Ok((input, Operation::Snd(register)))
}

fn operation(input: &str) -> IResult<&str, Operation> {
    alt((add, jgz, modulo, mul, set, rcv, snd))(input)
}

fn main() -> Result<()> {
    let mut stdin = std::io::stdin();

    let mut content = String::new();

    stdin.read_to_string(&mut content)?;

    let operations: Vec<Operation> = content
        .lines()
        .map(|line| {
            operation(line)
                .map(|(_, ops)| ops)
                .map_err(|error| anyhow!("Error parsing: {:?}", error))
        })
        .collect::<Result<_, _>>()?;

    let (first_context, second_context) = Context::new();
    let mut ready = vec![first_context, second_context];
    let mut waiting = Vec::with_capacity(2);
    loop {
        if let Some(mut context) = ready.pop() {
            let ops = &operations[context.instruction_counter];
            if !ops.execute(&mut context) {
                waiting.push(context);
            } else {
                ready.push(context);
            }
        } else {
            let mut i = 0;
            while i < waiting.len() {
                if waiting[i].rcv.borrow().is_empty() {
                    i += 1;
                } else {
                    let context = waiting.remove(i);
                    ready.push(context);
                }
            }

            if ready.is_empty() {
                break;
            }
        }
    }

    for context in waiting {
        if !context.is_zero {
            println!("{}", context.snd_counter);
        }
    }

    Ok(())
}
