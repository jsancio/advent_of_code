use anyhow::Result;
use core::fmt::Write;
use std::io::Read;

struct Hash {
    hash: Box<[u8]>,
    position: usize,
    skip: usize,
}

impl Hash {
    fn new() -> Self {
        Self {
            hash: (0..=255).collect(),
            position: 0,
            skip: 0,
        }
    }

    fn hash(mut self, text: &[u8]) -> String {
        // Knot hash the text 64 times
        let text_extension = [17, 31, 73, 47, 23];
        for _ in 0..64 {
            for byte in text.iter().chain(text_extension.iter()) {
                self.hash_with_length((*byte).into())
            }
        }

        self.xor();

        self.hex()
    }

    fn hash_with_length(&mut self, length: usize) {
        assert!(
            length <= self.hash.len(),
            "length ({}) is greater than {}",
            length,
            self.hash.len()
        );
        reverse(&mut self.hash, self.position, length);
        self.position = (self.position + length + self.skip) % 256;
        self.skip += 1;
    }

    fn xor(&mut self) {
        // XOR the 16 blocks of 16 u8
        let mut result = Vec::with_capacity(16);
        for chunk in self.hash.chunks_exact_mut(16) {
            let xor = chunk.iter_mut().fold(0, |state, item| state ^ *item);

            result.push(xor);
        }

        self.hash = result.into();
    }

    fn hex(&mut self) -> String {
        let mut string = String::with_capacity(2 * self.hash.len());

        for byte in self.hash.iter() {
            write!(&mut string, "{:x}", *byte).expect("Error occurred when writing to String");
        }

        string
    }
}

fn main() -> Result<()> {
    let stdin = std::io::stdin();

    let mut content = Vec::new();

    stdin.lock().read_to_end(&mut content)?;
    // Remove the trailing newline characters
    content.retain(|byte| *byte != b'\n');

    let hash = Hash::new().hash(&mut content);

    println!("{}", hash);

    Ok(())
}

fn reverse<T>(src: &mut [T], start: usize, length: usize) {
    assert!(
        start < src.len(),
        "start ({}) must be less than slice length ({})",
        start,
        src.len()
    );
    assert!(
        length <= src.len(),
        "length ({}) must be less than or equal to slice length ({})",
        length,
        src.len()
    );

    for i in 0..length / 2 {
        let first = (start + i) % src.len();
        let end = (start + length - i - 1) % src.len();

        src.swap(first, end);
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_basic_reverse() {
        let mut input = [1, 2, 3];
        reverse(&mut input, 0, 3);
        assert_eq!(input, [3, 2, 1]);
    }

    #[test]
    fn test_even_length() {
        let mut input = [1, 2, 3];
        reverse(&mut input, 2, 2);
        assert_eq!(input, [3, 2, 1]);
    }

    #[test]
    fn test_overlapping() {
        let mut input = [1, 2, 3];
        reverse(&mut input, 2, 3);
        assert_eq!(input, [1, 3, 2]);
    }

    #[test]
    fn test_unit_length() {
        let mut input = [1, 2, 3];
        reverse(&mut input, 2, 1);
        assert_eq!(input, [1, 2, 3]);
    }
}
