use anyhow::Result;
use bitvec::order::Msb0;
use bitvec::slice::AsBits;
use bitvec::vec::BitVec;
use core::convert::TryInto;

fn reverse<T>(src: &mut [T], start: usize, length: usize) {
    assert!(
        start < src.len(),
        "start ({}) must be less than slice length ({})",
        start,
        src.len()
    );
    assert!(
        length <= src.len(),
        "length ({}) must be less than or equal to slice length ({})",
        length,
        src.len()
    );

    for i in 0..length / 2 {
        let first = (start + i) % src.len();
        let end = (start + length - i - 1) % src.len();

        src.swap(first, end);
    }
}

struct Hash {
    hash: Box<[u8]>,
    position: usize,
    skip: usize,
}

impl Hash {
    fn new() -> Self {
        Self {
            hash: (0..=255).collect(),
            position: 0,
            skip: 0,
        }
    }

    fn hash(mut self, text: &[u8]) -> [u8; 16] {
        // Knot hash the text 64 times
        let text_extension = [17, 31, 73, 47, 23];
        for _ in 0..64 {
            for byte in text.iter().chain(text_extension.iter()) {
                self.hash_with_length((*byte).into())
            }
        }

        self.xor();

        self.hash.as_ref().try_into().unwrap()
    }

    fn hash_with_length(&mut self, length: usize) {
        assert!(
            length <= self.hash.len(),
            "length ({}) is greater than {}",
            length,
            self.hash.len()
        );
        reverse(&mut self.hash, self.position, length);
        self.position = (self.position + length + self.skip) % 256;
        self.skip += 1;
    }

    fn xor(&mut self) {
        // XOR the 16 blocks of 16 u8
        let mut result = Vec::with_capacity(16);
        for chunk in self.hash.chunks_exact_mut(16) {
            let xor = chunk.iter_mut().fold(0, |state, item| state ^ *item);

            result.push(xor);
        }

        self.hash = result.into();
    }
}

fn adjacent_indices(index: usize, rows: usize, columns: usize, to_visit: &mut Vec<usize>) {
    // Enumerate all of the adjacent bits
    if index % rows < columns - 1 {
        // to the right
        to_visit.push(index + 1)
    }
    if index % rows > 0 {
        // to the left
        to_visit.push(index - 1)
    }
    if index / rows < rows - 1 {
        // below
        to_visit.push(index + columns)
    }
    if index / rows > 0 {
        // above
        to_visit.push(index - columns)
    }
}

fn get_and_clear(bits: &mut BitVec, index: usize) -> bool {
    // Get the value at index and set it to zero/false
    let mut bit = bits.get_mut(index).unwrap();
    let old = *bit;
    *bit = false;

    old
}

fn main() -> Result<()> {
    let prefix = "ljoxqyyw";

    let mut bits: BitVec = BitVec::new();
    for row in 0..128 {
        let row_input = format!("{}-{}", prefix, row);
        let hash = Hash::new().hash(row_input.as_bytes());

        bits.extend_from_slice(hash.bits::<Msb0>())
    }

    let mut groups = 0;
    let mut to_visit = Vec::new();
    for i in 0..bits.len() {
        if get_and_clear(&mut bits, i) {
            // It is a new group
            groups += 1;
            adjacent_indices(i, 128, 128, &mut to_visit);

            loop {
                if let Some(current) = to_visit.pop() {
                    if get_and_clear(&mut bits, current) {
                        adjacent_indices(current, 128, 128, &mut to_visit);
                    }
                } else {
                    break;
                }
            }
        }
    }

    println!("{}", groups);

    Ok(())
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_adjacent_indices_start() {
        let mut to_visit = Vec::new();
        adjacent_indices(0, 128, 128, &mut to_visit);

        assert_eq!(to_visit.len(), 2);
        assert!(to_visit.contains(&1));
        assert!(to_visit.contains(&128));
    }

    #[test]
    fn test_adjacent_indices_1() {
        let mut to_visit = Vec::new();
        adjacent_indices(1, 128, 128, &mut to_visit);

        assert_eq!(to_visit.len(), 3);
        assert!(to_visit.contains(&2));
        assert!(to_visit.contains(&129));
        assert!(to_visit.contains(&0));
    }

    #[test]
    fn test_adjacent_indices_middle() {
        let mut to_visit = Vec::new();
        adjacent_indices(8200, 128, 128, &mut to_visit);

        assert_eq!(to_visit.len(), 4);
        assert!(to_visit.contains(&8201));
        assert!(to_visit.contains(&8328));
        assert!(to_visit.contains(&8199));
        assert!(to_visit.contains(&8072));
    }

    #[test]
    fn test_adjacent_indices_end() {
        let mut to_visit = Vec::new();
        adjacent_indices(16383, 128, 128, &mut to_visit);

        assert_eq!(to_visit.len(), 2);
        assert!(to_visit.contains(&16382));
        assert!(to_visit.contains(&16255));
    }
}
