use anyhow::Result;

struct Generator {
    prev: u64,
    factor: u64,
    multiple: u64,
}

impl Generator {
    fn new(initial: u64, factor: u64, multiple: u64) -> Self {
        Self {
            prev: initial,
            factor,
            multiple,
        }
    }
}

impl Iterator for Generator {
    type Item = u64;

    fn next(&mut self) -> Option<Self::Item> {
        loop {
            const MODULUS: u64 = 2147483647;
            self.prev = (self.prev * self.factor) % MODULUS;

            if self.prev % self.multiple == 0 {
                break Some(self.prev);
            }
        }
    }
}

fn match_lowest_bits(a: u64, b: u64) -> bool {
    const MASK: u64 = 0xffff;
    let a_lowest = a & MASK;
    let b_lowest = b & MASK;

    a_lowest == b_lowest
}

fn count_lowest_bit_matches<A, B>(gen_a: A, gen_b: B, take: usize) -> usize
where
    A: Iterator<Item = u64>,
    B: Iterator<Item = u64>,
{
    let mut count = 0;
    for (a, b) in gen_a.zip(gen_b).take(take) {
        if match_lowest_bits(a, b) {
            count += 1;
        }
    }

    count
}

fn main() -> Result<()> {
    let gen_a = Generator::new(679, 16807, 4);
    let gen_b = Generator::new(771, 48271, 8);

    println!("{}", count_lowest_bit_matches(gen_a, gen_b, 5_000_000));

    Ok(())
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_generator() {
        let gen = Generator::new(65, 16807, 4);
        let expected = [1352636452, 1992081072, 530830436, 1980017072, 740335192];

        for (g, e) in gen.zip(expected.iter()).take(5) {
            assert_eq!(g, *e);
        }
    }

    #[test]
    fn test_match_lowest_bits() {
        let a = 245556042;
        let b = 1431495498;

        assert!(match_lowest_bits(a, b));
    }

    #[test]
    fn test_count_lowest_bit_match() {
        let gen_a = Generator::new(65, 16807, 4);
        let gen_b = Generator::new(8921, 48271, 8);

        assert_eq!(count_lowest_bit_matches(gen_a, gen_b, 5_000_000), 309);
    }
}
