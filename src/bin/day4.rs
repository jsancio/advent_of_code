use std::io;
use std::io::BufRead;

fn main() -> io::Result<()> {
    let handle = io::stdin();

    let count: io::Result<i32> = handle.lock().lines().try_fold(0, |acc, line| {
        let line = line?;
        let mut words: Vec<&str> = line.split_whitespace().collect();
        words.sort_unstable();

        let valid = words.windows(2).all(|window| window[0] != window[1]);

        if valid {
            Ok(acc + 1)
        } else {
            Ok(acc)
        }
    });

    println!("{}", count?);

    Ok(())
}
