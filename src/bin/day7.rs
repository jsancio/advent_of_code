#[macro_use]
extern crate nom;

use nom::character::complete::alpha1;
use nom::character::complete::digit1;
use std::collections::HashMap;
use std::io;
use std::io::BufRead;
use std::str::FromStr;

#[derive(Debug)]
struct Node {
    weight: u32,
    children: Vec<String>,
}

impl Node {
    fn new(weight: u32, children: Vec<String>) -> Node {
        Node { weight, children }
    }
}

named! {
    unsigned_integer( &str ) -> u32,
    map_res!(digit1, FromStr::from_str)
}

named! {
    child( &str ) -> String,
    map!(alpha1, String::from)
}

named! {
    program_children( &str ) -> Vec<String>,
    do_parse!(
        complete!(tag!(" -> ")) >>
        children: separated_list!(complete!(tag!(", ")), child) >>
        (children)
    )
}

named! {
    program_node( &str ) -> (String, u32, Vec<String>),
    do_parse!(
        name: alpha1 >>
        tag!(" ") >>
        weight: delimited!(char!('('), unsigned_integer, char!(')')) >>
        children: opt!(program_children) >>

        (
            (
                String::from(name),
                weight,
                children.unwrap_or_default()
            )
        )
    )
}

fn main() -> io::Result<()> {
    let handle = io::stdin();

    let programs: io::Result<HashMap<String, Node>> =
        handle
            .lock()
            .lines()
            .try_fold(HashMap::new(), |mut acc, line| {
                let line = line?;
                let (_, (name, weight, children)) = program_node(&line).map_err(|_| {
                    io::Error::new(
                        io::ErrorKind::Other,
                        format!("Unable to parse line: {}", line),
                    )
                })?;

                acc.insert(name, Node::new(weight, children));

                Ok(acc)
            });
    let programs = programs?;

    let values: Vec<&str> = programs
        .values()
        .flat_map(|v| v.children.iter().map(|i| i.as_str()))
        .collect();
    let root = programs.keys().find(|&key| !values.contains(&key.as_str()));

    println!("{}", root.unwrap());

    Ok(())
}
