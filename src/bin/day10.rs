use anyhow::Context;
use anyhow::Result;
use std::io::Read;

struct Hasher {
    hash: Box<[u8]>,
    position: usize,
    skip: usize,
}

impl Hasher {
    fn new() -> Self {
        Self {
            hash: (0..=255).collect(),
            position: 0,
            skip: 0,
        }
    }

    fn hash(&mut self, length: usize) {
        assert!(
            length <= self.hash.len(),
            "legnth ({}) is greater than {}",
            length,
            self.hash.len()
        );
        reverse(&mut self.hash, self.position, length);
        self.position = (self.position + length + self.skip) % 256;
        self.skip += 1;
    }

    fn done(self) -> u16 {
        u16::from(self.hash[0]) * u16::from(self.hash[1])
    }
}

fn main() -> Result<()> {
    let stdin = std::io::stdin();

    let mut content = String::new();

    stdin.lock().read_to_string(&mut content)?;

    let mut hasher = Hasher::new();
    for number in content.trim().split(',') {
        let length: usize = number
            .parse()
            .with_context(|| format!("Unable to parse '{}'", number))?;
        hasher.hash(length);
    }

    println!("{}", hasher.done());

    Ok(())
}

fn reverse<T>(src: &mut [T], start: usize, length: usize) {
    assert!(
        start < src.len(),
        "start ({}) must be less than slice length ({})",
        start,
        src.len()
    );
    assert!(
        length <= src.len(),
        "length ({}) must be less than or equal to slice length ({})",
        length,
        src.len()
    );

    for i in 0..length / 2 {
        let first = (start + i) % src.len();
        let end = (start + length - i - 1) % src.len();

        src.swap(first, end);
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_basic_reverse() {
        let mut input = [1, 2, 3];
        reverse(&mut input, 0, 3);
        assert_eq!(input, [3, 2, 1]);
    }

    #[test]
    fn test_even_length() {
        let mut input = [1, 2, 3];
        reverse(&mut input, 2, 2);
        assert_eq!(input, [3, 2, 1]);
    }

    #[test]
    fn test_overlapping() {
        let mut input = [1, 2, 3];
        reverse(&mut input, 2, 3);
        assert_eq!(input, [1, 3, 2]);
    }

    #[test]
    fn test_unit_length() {
        let mut input = [1, 2, 3];
        reverse(&mut input, 2, 1);
        assert_eq!(input, [1, 2, 3]);
    }
}
