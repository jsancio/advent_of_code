use anyhow::anyhow;
use anyhow::Error;
use anyhow::Result;
use core::str::FromStr;
use nom::bytes::complete::tag;
use nom::bytes::complete::take_while;
use nom::combinator::map_res;
use nom::multi::separated_list;
use nom::IResult;
use std::collections::HashMap;
use std::collections::HashSet;
use std::io::Read;
use std::iter::FromIterator;

fn program_id(input: &str) -> IResult<&str, u16> {
    map_res(take_while(|c: char| c.is_ascii_digit()), u16::from_str)(input)
}

fn connection_ids(input: &str) -> IResult<&str, HashSet<u16>> {
    let (input, ids) = separated_list(tag(", "), program_id)(input)?;

    Ok((input, HashSet::from_iter(ids.into_iter())))
}

fn pipeline(input: &str) -> IResult<&str, (u16, HashSet<u16>)> {
    let (input, id) = program_id(input)?;
    let (input, _) = tag(" <-> ")(input)?;
    let (input, connected_ids) = connection_ids(input)?;

    Ok((input, (id, connected_ids)))
}

struct Pluming {
    graph: HashMap<u16, HashSet<u16>>,
}

impl Pluming {
    fn new() -> Self {
        Self {
            graph: Default::default(),
        }
    }

    fn put_connection(&mut self, id: u16, connections: HashSet<u16>) {
        self.graph.insert(id, connections);
    }

    fn connected_ids(&self, source: u16) -> HashSet<u16> {
        let mut visited: HashSet<u16> = HashSet::new();

        let mut to_visit = HashSet::new();
        to_visit.insert(source);

        while !to_visit.is_empty() {
            let mut ids = to_visit;
            to_visit = HashSet::new();

            for id in ids.drain() {
                if !visited.contains(&id) {
                    if let Some(connections) = self.graph.get(&id) {
                        to_visit.extend(connections.iter());
                    }
                    visited.insert(id);
                }
            }
        }

        visited
    }
}

impl FromStr for Pluming {
    type Err = Error;

    fn from_str(input: &str) -> Result<Self> {
        let mut output = Self::new();
        for line in input.lines() {
            let (_, (id, connected_ids)) =
                pipeline(line).map_err(|error| anyhow!("Error parsing: {:?}", error))?;

            output.put_connection(id, connected_ids);
        }

        Ok(output)
    }
}

fn main() -> Result<()> {
    let mut stdin = std::io::stdin();

    let mut string = String::new();

    stdin.read_to_string(&mut string)?;

    let pluming: Pluming = string.parse()?;

    println!("{}", pluming.connected_ids(0).len());

    Ok(())
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_parser() {
        let input = "1910 <-> 1353\n1911 <-> 686, 795, 1246\n";

        let pluming: Pluming = input.parse().unwrap();
        assert!(pluming.graph.get(&1910).unwrap().contains(&1353));
        assert!(pluming.graph.get(&1911).unwrap().contains(&686));
        assert!(pluming.graph.get(&1911).unwrap().contains(&795));
        assert!(pluming.graph.get(&1911).unwrap().contains(&1246));
    }

    #[test]
    fn test_connected_ids() {
        let input =
            "0 <-> 2\n1 <-> 1\n2 <-> 0, 3, 4\n3 <-> 2, 4\n4 <-> 2, 3, 6\n5 <-> 6\n6 <-> 4, 5";

        let pluming: Pluming = input.parse().unwrap();
        assert_eq!(
            pluming.connected_ids(0),
            HashSet::from_iter([0, 2, 3, 4, 5, 6].iter().cloned())
        );
    }
}
