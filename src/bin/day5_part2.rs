use std::io;
use std::io::BufRead;

fn main() -> io::Result<()> {
    let handle = io::stdin();

    let jumps: io::Result<Vec<i32>> =
        handle.lock().lines().try_fold(Vec::new(), |mut acc, line| {
            let line = line?;
            let jump = line.parse::<i32>().map_err(|_| {
                io::Error::new(io::ErrorKind::Other, format!("{} is not a digit!", line))
            })?;

            acc.push(jump);
            Ok(acc)
        });
    let mut jumps = jumps?;

    let mut index = 0;
    let mut steps = 0;
    while index < jumps.len() {
        let jump = jumps[index];
        if jump >= 3 {
            jumps[index] = jump - 1;
        } else {
            jumps[index] = jump + 1;
        }

        if jump < 0 {
            index -= (-jump) as usize
        } else {
            index += jump as usize;
        }

        steps += 1;
    }

    println!("{}", steps);

    Ok(())
}
