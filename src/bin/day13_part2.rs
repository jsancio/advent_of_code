use anyhow::anyhow;
use anyhow::Error;
use anyhow::Result;
use core::str::FromStr;
use nom::bytes::complete::tag;
use nom::character::complete::digit1;
use nom::combinator::map_res;
use nom::IResult;
use std::io::Read;

#[derive(Debug)]
struct Firewall {
    scanners: Vec<(u8, u8)>,
}

impl Firewall {
    fn new() -> Self {
        Self {
            scanners: Vec::new(),
        }
    }

    fn add_scanner(&mut self, depth: u8, range: u8) {
        self.scanners.push((depth, range));
    }

    fn caught(&self, delay: usize) -> bool {
        for (depth, range) in self.scanners.iter().cloned() {
            if is_scanner_at_0(usize::from(depth) + delay, range) {
                return true;
            }
        }

        false
    }

    fn delay_until_zero_severity(&self) -> usize {
        let mut delay = 0;
        loop {
            if !self.caught(delay) {
                break delay;
            }

            delay += 1;
        }
    }
}

fn is_scanner_at_0(time: usize, range: u8) -> bool {
    if range == 0 {
        return false;
    }

    let loop_time = usize::from(range + range - 2);
    if loop_time == 0 {
        true
    } else {
        time % loop_time == 0
    }
}

fn number(input: &str) -> IResult<&str, u8> {
    map_res(digit1, u8::from_str)(input)
}

fn scanner(input: &str) -> IResult<&str, (u8, u8)> {
    let (input, depth) = number(input)?;
    let (input, _) = tag(": ")(input)?;
    let (input, range) = number(input)?;

    Ok((input, (depth, range)))
}

impl FromStr for Firewall {
    type Err = Error;

    fn from_str(input: &str) -> Result<Self> {
        let mut output = Self::new();

        for line in input.lines() {
            let (_, (depth, range)) =
                scanner(line).map_err(|error| anyhow!("Error parsing: {:?}", error))?;
            output.add_scanner(depth, range);
        }

        Ok(output)
    }
}

fn main() -> Result<()> {
    let mut stdin = std::io::stdin();
    let mut string = String::new();

    stdin.read_to_string(&mut string)?;
    let firewall = Firewall::from_str(&string)?;

    println!("{}", firewall.delay_until_zero_severity());

    Ok(())
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_scanner_range_0() {
        assert_eq!(is_scanner_at_0(0, 0), false);
        assert_eq!(is_scanner_at_0(1, 0), false);
        assert_eq!(is_scanner_at_0(2, 0), false);
    }

    #[test]
    fn test_scanner_range_1() {
        assert_eq!(is_scanner_at_0(0, 1), true);
        assert_eq!(is_scanner_at_0(1, 1), true);
        assert_eq!(is_scanner_at_0(2, 1), true);
        assert_eq!(is_scanner_at_0(3, 1), true);
    }

    #[test]
    fn test_scanner_range_2() {
        assert_eq!(is_scanner_at_0(0, 2), true);
        assert_eq!(is_scanner_at_0(1, 2), false);
        assert_eq!(is_scanner_at_0(2, 2), true);
        assert_eq!(is_scanner_at_0(3, 2), false);
        assert_eq!(is_scanner_at_0(4, 2), true);
        assert_eq!(is_scanner_at_0(5, 2), false);
        assert_eq!(is_scanner_at_0(6, 2), true);
    }

    #[test]
    fn test_scanner_range_3() {
        assert_eq!(is_scanner_at_0(0, 3), true);
        assert_eq!(is_scanner_at_0(1, 3), false);
        assert_eq!(is_scanner_at_0(2, 3), false);
        assert_eq!(is_scanner_at_0(3, 3), false);
        assert_eq!(is_scanner_at_0(4, 3), true);
        assert_eq!(is_scanner_at_0(5, 3), false);
        assert_eq!(is_scanner_at_0(6, 3), false);
    }

    #[test]
    fn test_scanner_range_4() {
        assert_eq!(is_scanner_at_0(0, 4), true);
        assert_eq!(is_scanner_at_0(1, 4), false);
        assert_eq!(is_scanner_at_0(2, 4), false);
        assert_eq!(is_scanner_at_0(3, 4), false);
        assert_eq!(is_scanner_at_0(4, 4), false);
        assert_eq!(is_scanner_at_0(5, 4), false);
        assert_eq!(is_scanner_at_0(6, 4), true);
    }
}
