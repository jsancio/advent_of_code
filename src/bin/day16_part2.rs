use anyhow::anyhow;
use anyhow::Result;
use core::str::FromStr;
use nom::branch::alt;
use nom::bytes::complete::tag;
use nom::character::complete::digit1;
use nom::combinator::map_res;
use nom::multi::separated_list;
use nom::IResult;
use std::io::Read;

#[derive(Debug, PartialEq)]
enum Command {
    Spin(u8),
    IndexSwap(u8, u8),
    NameSwap(u8, u8),
}

impl Command {
    fn dance(&self, programs: &mut [u8]) {
        match self {
            &Command::Spin(spin) => Command::spin_programs(programs, spin.into()),
            &Command::IndexSwap(first, second) => {
                Command::index_swap(programs, first.into(), second.into())
            }
            &Command::NameSwap(first, second) => Command::name_swap(programs, first, second),
        }
    }

    fn spin_programs(programs: &mut [u8], spin: usize) {
        programs.rotate_right(spin)
    }

    fn index_swap(programs: &mut [u8], first: usize, second: usize) {
        programs.swap(first, second)
    }

    fn name_swap(programs: &mut [u8], first: u8, second: u8) {
        for value in programs {
            if *value == first {
                *value = second;
            } else if *value == second {
                *value = first;
            }
        }
    }
}

fn number(input: &str) -> IResult<&str, u8> {
    map_res(digit1, u8::from_str)(input)
}

fn spin_command(input: &str) -> IResult<&str, Command> {
    let (input, _) = tag("s")(input)?;
    let (input, size) = number(input)?;

    Ok((input, Command::Spin(size)))
}

fn index_swap_command(input: &str) -> IResult<&str, Command> {
    let (input, _) = tag("x")(input)?;
    let (input, first) = number(input)?;
    let (input, _) = tag("/")(input)?;
    let (input, second) = number(input)?;

    Ok((input, Command::IndexSwap(first, second)))
}

fn name_swap_command(input: &str) -> IResult<&str, Command> {
    let (input, _) = tag("p")(input)?;
    let (first, input) = input.split_at(1);
    let first = first.as_bytes()[0];
    let (input, _) = tag("/")(input)?;
    let (second, input) = input.split_at(1);
    let second = second.as_bytes()[0];

    Ok((input, Command::NameSwap(first, second)))
}

fn command(input: &str) -> IResult<&str, Command> {
    alt((spin_command, index_swap_command, name_swap_command))(input)
}

fn commands(input: &str) -> IResult<&str, Vec<Command>> {
    separated_list(tag(","), command)(input)
}

fn main() -> Result<()> {
    let mut stdin = std::io::stdin();

    let mut content = String::new();

    stdin.read_to_string(&mut content)?;

    let (_, all_commands) =
        commands(&content).map_err(|error| anyhow!("Error parsing: {:?}", error))?;
    let mut programs: Vec<u8> = (b'a'..=b'p').collect();
    let original = programs.clone();

    let mut i = 0;
    let mut cycle = 0;
    while i < 1_000_000_000 {
        if cycle == 0 {
            for a_command in all_commands.iter() {
                a_command.dance(&mut programs);
            }
            i += 1;

            if programs == original {
                cycle = i;
                println!("cycle: {}", cycle);
                println!("original: {:?}", original);
                println!("programs: {:?}", programs);
            }
        } else if i + cycle > 1_000_000_000 {
            for a_command in all_commands.iter() {
                a_command.dance(&mut programs);
            }
            i += 1;
        } else {
            i += cycle;
        }
    }

    println!("{}", std::str::from_utf8(&programs)?);

    Ok(())
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_spin_command() {
        assert_eq!(spin_command("s10").unwrap(), ("", Command::Spin(10)));
    }

    #[test]
    fn test_failed_spin_command() {
        assert!(spin_command("a10").is_err());
    }

    #[test]
    fn test_index_swap_command() {
        assert_eq!(
            index_swap_command("x10/5").unwrap(),
            ("", Command::IndexSwap(10, 5))
        );
    }

    #[test]
    fn test_failed_index_swap_command() {
        assert!(index_swap_command("p10/5").is_err());
    }

    #[test]
    fn test_name_swap_command() {
        assert_eq!(
            name_swap_command("pe/f").unwrap(),
            ("", Command::NameSwap(b'e', b'f'))
        );
    }

    #[test]
    fn test_failed_name_swap_command() {
        assert!(name_swap_command("se/f").is_err());
    }

    #[test]
    fn test_command() {
        assert_eq!(
            command("pe/f").unwrap(),
            ("", Command::NameSwap(b'e', b'f'))
        );
        assert_eq!(command("x10/5").unwrap(), ("", Command::IndexSwap(10, 5)));
        assert_eq!(command("s10").unwrap(), ("", Command::Spin(10)));
    }

    #[test]
    fn test_failed_command() {
        assert!(command("a10").is_err());
    }

    #[test]
    fn test_commands() {
        let (_, result) = commands("pe/f,x10/5,s10").unwrap();

        assert_eq!(
            result,
            vec![
                Command::NameSwap(b'e', b'f'),
                Command::IndexSwap(10, 5),
                Command::Spin(10)
            ]
        );
    }

    #[test]
    fn test_spin_dance() {
        let mut programs = b"abcde".to_owned();
        Command::Spin(1).dance(&mut programs);

        assert_eq!(&programs, b"eabcd");
    }

    #[test]
    fn test_index_swap_dance() {
        let mut programs = b"eabcd".to_owned();
        Command::IndexSwap(3, 4).dance(&mut programs);

        assert_eq!(&programs, b"eabdc");
    }

    #[test]
    fn test_name_swap_dance() {
        let mut programs = b"eabdc".to_owned();
        Command::NameSwap(b'e', b'b').dance(&mut programs);

        assert_eq!(&programs, b"baedc");
    }
}
