#[macro_use]
extern crate nom;

use nom::character::complete::alpha1;
use nom::character::complete::digit1;
use std::collections::HashMap;
use std::io;
use std::io::BufRead;
use std::str::FromStr;

#[derive(Debug)]
struct Node {
    weight: u32,
    children: Vec<String>,
}

impl Node {
    fn new(weight: u32, children: Vec<String>) -> Node {
        Node { weight, children }
    }
}

named! {
    unsigned_integer( &str ) -> u32,
    map_res!(digit1, FromStr::from_str)
}

named! {
    child( &str ) -> String,
    map!(alpha1, String::from)
}

named! {
    program_children( &str ) -> Vec<String>,
    do_parse!(
        complete!(tag!(" -> ")) >>
        children: separated_list!(complete!(tag!(", ")), child) >>
        (children)
    )
}

named! {
    program_node( &str ) -> (String, u32, Vec<String>),
    do_parse!(
        name: alpha1 >>
        tag!(" ") >>
        weight: delimited!(char!('('), unsigned_integer, char!(')')) >>
        children: opt!(program_children) >>

        (
            (
                String::from(name),
                weight,
                children.unwrap_or_default()
            )
        )
    )
}

fn main() -> io::Result<()> {
    let handle = io::stdin();

    let programs: io::Result<HashMap<String, Node>> =
        handle
            .lock()
            .lines()
            .try_fold(HashMap::new(), |mut acc, line| {
                let line = line?;
                let (_, (name, weight, children)) = program_node(&line).map_err(|_| {
                    io::Error::new(
                        io::ErrorKind::Other,
                        format!("Unable to parse line: {}", line),
                    )
                })?;

                acc.insert(name, Node::new(weight, children));

                Ok(acc)
            });
    let programs = programs?;

    let values: Vec<&str> = programs
        .values()
        .flat_map(|v| v.children.iter().map(|i| i.as_str()))
        .collect();
    let root = programs.keys().find(|&key| !values.contains(&key.as_str()));

    println!("{}", off_balance(&programs, root.unwrap()).unwrap());

    Ok(())
}

#[derive(Debug)]
enum Either {
    Weight(u32, u32),
    MissingWeight(u32),
}

fn off_balance(programs: &HashMap<String, Node>, root: &str) -> Option<u32> {
    match off_balance_loop(programs, root) {
        Either::MissingWeight(missing) => Some(missing),
        _ => None,
    }
}

fn off_balance_loop(programs: &HashMap<String, Node>, root: &str) -> Either {
    let program = &programs[root];

    let results = program
        .children
        .iter()
        .map(|name| off_balance_loop(programs, name));

    let maybe_weights = results
        .clone()
        .fold(Ok(Vec::new()), |acc, result| match result {
            Either::Weight(root, children) => acc.map(|mut weights| {
                weights.push((root, children));
                weights
            }),
            Either::MissingWeight(missing) => Err(missing),
        });

    match maybe_weights {
        Ok(mut weights) => {
            weights.sort_unstable_by(|(a_root, a_children), (b_root, b_children)| {
                (a_root + a_children).cmp(&(b_root + b_children))
            });
            match missing_weight(&weights) {
                Some(missing_weight) => {
                    // TODO: I think this is wrong we are support to return the one that
                    // repeats.
                    Either::MissingWeight(missing_weight)
                }
                None => Either::Weight(
                    program.weight,
                    weights.iter().map(|(root, children)| root + children).sum(),
                ),
            }
        }
        Err(missing) => Either::MissingWeight(missing),
    }
}

fn missing_weight(slice: &[(u32, u32)]) -> Option<u32> {
    let (unique, duped, _) = slice.iter().fold((None, None, None), |acc, new| {
        let (new_root, new_children) = new;
        match acc {
            // Already found a unique item
            (Some(x), _, _) => (Some(x), Some(*new), Some(*new)),
            (None, None, Some((last_root, last_children))) => {
                if last_root + last_children == new_root + new_children {
                    // Item repeats so it is not different
                    (None, Some(*new), Some(*new))
                } else {
                    // Item do not match last value must be unique; new value must repeat
                    (Some((last_root, last_children)), Some(*new), Some(*new))
                }
            }
            (None, Some(duped), Some((last_root, last_children))) => {
                if last_root + last_children == new_root + new_children {
                    // Item repeats so it is not different
                    (None, Some(duped), Some(*new))
                } else {
                    // Item do not match last value must be unique; new value must repeat
                    (Some((*new_root, *new_children)), Some(duped), Some(*new))
                }
            }
            // First iteration; remember the last value
            (_, _, None) => (None, None, Some(*new)),
        }
    });

    match (unique, duped) {
        (Some((_, children)), Some((duped_root, duped_children))) => {
            Some(duped_root + duped_children - children)
        }
        _ => None,
    }
}
