#[macro_use]
extern crate nom;

use nom::character::complete::alpha1;
use nom::character::complete::digit1;
use nom::IResult;
use std::collections::HashMap;
use std::io;
use std::io::BufRead;
use std::str::FromStr;

trait Operation {
    type Item;
    fn eval(&self, registries: &mut HashMap<String, i32>, registry: &str, value: i32)
        -> Self::Item;
}

#[derive(Debug)]
enum WriteOperation {
    Increment,
    Decrement,
}

impl Operation for WriteOperation {
    type Item = ();

    fn eval(&self, registries: &mut HashMap<String, i32>, registry: &str, value: i32) {
        let sign = match self {
            WriteOperation::Increment => 1,
            WriteOperation::Decrement => -1,
        };

        let registry_value = registries.entry(String::from(registry)).or_insert(0);
        *registry_value += sign * value;
    }
}

#[derive(Debug, Copy, Clone)]
enum CmpOperation {
    LessThan,
    GreaterThan,
    LessThanOrEqual,
    GreaterThanOrEqual,
    Equal,
    NotEqual,
}

impl Operation for CmpOperation {
    type Item = bool;

    fn eval(&self, registries: &mut HashMap<String, i32>, registry: &str, value: i32) -> bool {
        let registry_value = *registries.entry(String::from(registry)).or_insert(0);

        match self {
            CmpOperation::LessThan => registry_value < value,
            CmpOperation::GreaterThan => registry_value > value,
            CmpOperation::LessThanOrEqual => registry_value <= value,
            CmpOperation::GreaterThanOrEqual => registry_value >= value,
            CmpOperation::Equal => registry_value == value,
            CmpOperation::NotEqual => registry_value != value,
        }
    }
}

#[derive(Debug)]
struct Expression<T> {
    registry: String,
    operation: T,
    value: i32,
}

impl<T: Operation> Expression<T> {
    fn new(registry: String, operation: T, value: i32) -> Expression<T> {
        Expression {
            registry,
            operation,
            value,
        }
    }

    fn eval(&self, registries: &mut HashMap<String, i32>) -> T::Item {
        self.operation
            .eval(registries, self.registry.as_str(), self.value)
    }
}

#[derive(Debug)]
struct Instruction {
    write_expression: Expression<WriteOperation>,
    cmp_expression: Expression<CmpOperation>,
}

impl Instruction {
    fn new(
        write_expression: Expression<WriteOperation>,
        cmp_expression: Expression<CmpOperation>,
    ) -> Instruction {
        Instruction {
            write_expression,
            cmp_expression,
        }
    }

    fn eval(&self, registries: &mut HashMap<String, i32>) {
        if self.cmp_expression.eval(registries) {
            self.write_expression.eval(registries);
        }
    }
}

named! {
    unsigned_integer<&str, i32>,
    map_res!(digit1, FromStr::from_str)
}

named! {
   integer<&str, i32>,
   do_parse!(
       sign: opt!(tag!("-")) >>
       number: unsigned_integer >>
       (
           number * if sign.is_some() { -1 } else { 1 }
       )
   )
}

named! {
    write_operation<&str, WriteOperation>,
    map_res!(take!(3), |input| match input {
        "inc" => Ok(WriteOperation::Increment),
        "dec" => Ok(WriteOperation::Decrement),
        ops => Err(format!("Write operation {} not supported", ops)),
    })
}

named! {
    cmp_operation<&str, CmpOperation>,
    alt!(
        value!(CmpOperation::NotEqual, tag!("!=")) |
        value!(CmpOperation::Equal, tag!("==")) |
        value!(CmpOperation::LessThanOrEqual, tag!("<=")) |
        value!(CmpOperation::GreaterThanOrEqual, tag!(">=")) |
        value!(CmpOperation::LessThan, tag!("<")) |
        value!(CmpOperation::GreaterThan, tag!(">"))
    )
}

fn expression<T: Operation>(
    input: &str,
    parser: fn(&str) -> IResult<&str, T>,
) -> IResult<&str, Expression<T>> {
    do_parse!(
        input,
        registry: alpha1
            >> tag!(" ")
            >> operation: parser
            >> tag!(" ")
            >> number: integer
            >> (Expression::new(String::from(registry), operation, number))
    )
}

fn instruction(input: &str) -> IResult<&str, Instruction> {
    let write_expression = |input| expression(input, write_operation);
    let cmp_expression = |input| expression(input, cmp_operation);

    do_parse!(
        input,
        write: write_expression
            >> tag!(" if ")
            >> cmp: cmp_expression
            >> (Instruction::new(write, cmp))
    )
}

fn main() -> io::Result<()> {
    let handle = io::stdin();
    let mut buffer = String::new();
    let mut reader = handle.lock();
    let mut registries = HashMap::new();

    loop {
        match reader.read_line(&mut buffer) {
            Ok(0) => break Ok(()),
            Ok(_) => {
                let (_, ins) = instruction(buffer.as_str()).map_err(|_| {
                    io::Error::new(
                        io::ErrorKind::Other,
                        format!("Unable to parse line: '{}'", buffer),
                    )
                })?;
                buffer.clear();

                ins.eval(&mut registries);
            }
            Err(err) => break Err(err),
        }
    }?;

    println!("{}", registries.values().max().unwrap());

    Ok(())
}
