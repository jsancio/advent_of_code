use anyhow::Result;

fn main() -> Result<()> {
    const NUMBER_OF_ELEMENTS: usize = 50_000_000;
    const SKIP: usize = 366;

    let mut position = 0;
    let mut len = 1;
    let mut second = 0;
    for i in 1..NUMBER_OF_ELEMENTS {
        position = (position + SKIP) % len + 1;
        len += 1;

        if position == 1 {
            second = i;
        }
    }

    println!("{}", second);

    Ok(())
}
