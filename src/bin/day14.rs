use anyhow::Result;
use core::convert::TryInto;

fn reverse<T>(src: &mut [T], start: usize, length: usize) {
    assert!(
        start < src.len(),
        "start ({}) must be less than slice length ({})",
        start,
        src.len()
    );
    assert!(
        length <= src.len(),
        "length ({}) must be less than or equal to slice length ({})",
        length,
        src.len()
    );

    for i in 0..length / 2 {
        let first = (start + i) % src.len();
        let end = (start + length - i - 1) % src.len();

        src.swap(first, end);
    }
}

struct Hash {
    hash: Box<[u8]>,
    position: usize,
    skip: usize,
}

impl Hash {
    fn new() -> Self {
        Self {
            hash: (0..=255).collect(),
            position: 0,
            skip: 0,
        }
    }

    fn hash(mut self, text: &[u8]) -> u128 {
        // Knot hash the text 64 times
        let text_extension = [17, 31, 73, 47, 23];
        for _ in 0..64 {
            for byte in text.iter().chain(text_extension.iter()) {
                self.hash_with_length((*byte).into())
            }
        }

        self.xor();

        self.hex()
    }

    fn hash_with_length(&mut self, length: usize) {
        assert!(
            length <= self.hash.len(),
            "length ({}) is greater than {}",
            length,
            self.hash.len()
        );
        reverse(&mut self.hash, self.position, length);
        self.position = (self.position + length + self.skip) % 256;
        self.skip += 1;
    }

    fn xor(&mut self) {
        // XOR the 16 blocks of 16 u8
        let mut result = Vec::with_capacity(16);
        for chunk in self.hash.chunks_exact_mut(16) {
            let xor = chunk.iter_mut().fold(0, |state, item| state ^ *item);

            result.push(xor);
        }

        self.hash = result.into();
    }

    fn hex(&mut self) -> u128 {
        let array: [u8; 16] = self.hash.as_ref().try_into().unwrap();
        u128::from_be_bytes(array)
    }
}

fn main() -> Result<()> {
    let prefix = "ljoxqyyw";

    let mut used = 0;
    for row in 0..128 {
        let row_input = format!("{}-{}", prefix, row);
        let hash = Hash::new().hash(row_input.as_bytes());

        used += hash.count_ones();
    }

    println!("{}", used);

    Ok(())
}
