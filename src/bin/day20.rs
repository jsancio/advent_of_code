use anyhow::anyhow;
use anyhow::Result;
use nom::bytes::complete::tag;
use nom::character::complete::digit1;
use nom::combinator::map_res;
use nom::combinator::opt;
use nom::IResult;
use std::io::BufRead;
use std::str::FromStr;

fn number(input: &str) -> IResult<&str, i64> {
    let (input, sign) = opt(tag("-"))(input)?;
    let (input, number) = map_res(digit1, i64::from_str)(input)?;

    let sign = sign.map_or(1, |_| -1);

    Ok((input, sign * number))
}

fn vector_3d(input: &str) -> IResult<&str, Vector> {
    let (input, _) = tag("<")(input)?;

    let (input, x) = number(input)?;
    let (input, _) = tag(",")(input)?;

    let (input, y) = number(input)?;
    let (input, _) = tag(",")(input)?;

    let (input, z) = number(input)?;

    let (input, _) = tag(">")(input)?;

    Ok((input, Vector([x, y, z])))
}

fn particle(input: &str) -> IResult<&str, Particle> {
    let (input, _) = tag("p=")(input)?;
    let (input, p) = vector_3d(input)?;
    let (input, _) = tag(", ")(input)?;

    let (input, _) = tag("v=")(input)?;
    let (input, v) = vector_3d(input)?;
    let (input, _) = tag(", ")(input)?;

    let (input, _) = tag("a=")(input)?;
    let (input, a) = vector_3d(input)?;

    Ok((input, Particle::new(p, v, a)))
}

#[derive(Debug, PartialEq, Clone)]
struct Vector([i64; 3]);

impl Vector {
    fn tick(&mut self, derivative: &Vector) {
        self.0[0] += derivative.0[0];
        self.0[1] += derivative.0[1];
        self.0[2] += derivative.0[2];
    }

    fn abs(&self) -> i64 {
        self.0[0].abs() + self.0[1].abs() + self.0[2].abs()
    }

    fn x(&self) -> i64 {
        self.0[0]
    }

    fn y(&self) -> i64 {
        self.0[1]
    }

    fn z(&self) -> i64 {
        self.0[2]
    }
}

#[derive(Debug, PartialEq, Clone)]
struct Particle {
    position: Vector,
    velocity: Vector,
    acceleration: Vector,
}

impl Particle {
    fn new(position: Vector, velocity: Vector, acceleration: Vector) -> Self {
        Self {
            position,
            velocity,
            acceleration,
        }
    }

    fn tick(&mut self) {
        self.velocity.tick(&self.acceleration);
        self.position.tick(&self.velocity);
    }

    fn speed(&self) -> i64 {
        self.velocity.abs()
    }

    fn max_expansion(&self) -> bool {
        self.position.x().signum() == self.velocity.x().signum()
            && (self.position.x().signum() == self.acceleration.x().signum()
                || self.acceleration.x() == 0)
            && self.position.y().signum() == self.velocity.y().signum()
            && (self.position.y().signum() == self.acceleration.y().signum()
                || self.acceleration.y() == 0)
            && self.position.z().signum() == self.velocity.z().signum()
            && (self.position.z().signum() == self.acceleration.z().signum()
                || self.acceleration.z() == 0)
    }
}

#[derive(Debug)]
struct Swarm {
    particles: Vec<(usize, Particle)>,
}

impl Swarm {
    fn new(particles: Vec<(usize, Particle)>) -> Self {
        Self { particles }
    }

    fn tick(&mut self, mut closest: Option<(usize, Particle)>) -> Option<(usize, Particle)> {
        if let Some((_, closest)) = &mut closest {
            closest.tick();
        }

        for i in (0..self.particles.len()).rev() {
            let (_, particle) = &mut self.particles[i];
            particle.tick();

            if particle.max_expansion() {
                let (id, particle) = self.particles.swap_remove(i);
                if let Some((closest_id, closest_particle)) = closest.take() {
                    if particle.speed() < closest_particle.speed() {
                        closest = Some((id, particle));
                    } else {
                        closest = Some((closest_id, closest_particle));
                    }
                } else {
                    closest = Some((id, particle));
                }
            }
        }

        closest
    }

    fn find_shortest(&mut self) -> Option<(usize, Particle)> {
        let mut closest = None;
        while !self.particles.is_empty() {
            closest = self.tick(closest);
        }

        closest
    }

    fn find_slowest(&mut self) {
        let mut particles = Vec::new();
        std::mem::swap(&mut self.particles, &mut particles);

        let mut iter = particles.into_iter();
        self.particles.push(iter.next().unwrap());

        for (id, particle) in iter {
            if particle.acceleration.abs() <= self.particles[0].1.acceleration.abs() {
                if particle.acceleration.abs() < self.particles[0].1.acceleration.abs() {
                    self.particles.clear();
                }

                self.particles.push((id, particle));
            }
        }
    }
}

fn main() -> Result<()> {
    let stdin = std::io::stdin();

    let mut particles = Vec::new();
    for (i, line) in stdin.lock().lines().enumerate() {
        let (_, particle) = particle(&line?).map_err(|err| anyhow!("Error parsing: {:?}", err))?;
        particles.push((i, particle));
    }

    let mut swarm = Swarm::new(particles);

    swarm.find_slowest();

    let (id, _) = swarm.find_shortest().unwrap();

    println!("{}", id);

    Ok(())
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_particle_parser() {
        let (_, particle) =
            particle("p=<-1338,2295,4101>, v=<-158,34,-48>, a=<19,-12,-12>").unwrap();
        let expected = Particle::new(
            Vector([-1338, 2295, 4101]),
            Vector([-158, 34, -48]),
            Vector([19, -12, -12]),
        );

        assert_eq!(particle, expected)
    }

    #[test]
    fn test_vector_distance() {
        let vector = Vector([-10, 11, 0]);
        assert_eq!(vector.abs(), 21);
    }

    #[test]
    fn test_particle_speed() {
        let particle = Particle::new(
            Vector([-1338, 2295, 4101]),
            Vector([-158, 34, -48]),
            Vector([19, -12, -12]),
        );
        assert_eq!(particle.speed(), 249);
    }

    #[test]
    fn test_vector_tick() {
        let mut vector = Vector([-1338, 2295, 4101]);
        vector.tick(&Vector([-158, 34, -48]));

        assert_eq!(vector, Vector([-1496, 2329, 4053]));
    }

    #[test]
    fn test_particle_tick() {
        let mut particle = Particle::new(
            Vector([-1338, 2295, 4101]),
            Vector([-158, 34, -48]),
            Vector([19, -12, -12]),
        );
        particle.tick();

        let expected = Particle::new(
            Vector([-1477, 2317, 4041]),
            Vector([-139, 22, -60]),
            Vector([19, -12, -12]),
        );

        assert_eq!(particle, expected);
    }
}
