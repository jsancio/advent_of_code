use anyhow::Result;

fn main() -> Result<()> {
    const NUMBER_OF_ELEMENTS: usize = 2018;
    const SKIP: usize = 366;

    let mut buffer = Vec::with_capacity(NUMBER_OF_ELEMENTS);
    buffer.push(0);

    let mut position = 0;
    for i in 1..NUMBER_OF_ELEMENTS {
        position = (position + SKIP) % buffer.len() + 1;
        buffer.insert(position, i);
    }

    println!("{}", buffer[(position + 1) % buffer.len()]);

    Ok(())
}
