use std::io::{self, Read};

fn main() -> io::Result<()> {
    let mut buffer = String::new();
    let handle = io::stdin();

    handle.lock().read_to_string(&mut buffer)?;
    let buffer = buffer.into_bytes();

    let (left, right) = buffer.split_at(buffer.len() / 2);

    let result = left.iter().zip(right.iter()).try_fold(0, per)?;

    println!("{}", result);

    Ok(())
}

fn per(acc: u32, value: (&u8, &u8)) -> io::Result<u32> {
    let (left, right) = value;
    let left = char::from(*left)
        .to_digit(10)
        .ok_or_else(|| io::Error::new(io::ErrorKind::Other, "Not a digit!"))?;
    let right = char::from(*right)
        .to_digit(10)
        .ok_or_else(|| io::Error::new(io::ErrorKind::Other, "Not a digit!"))?;

    let diff = if left == right { left + right } else { 0 };

    Ok(acc + diff)
}
