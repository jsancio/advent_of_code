use std::io;
use std::io::Read;

fn main() -> io::Result<()> {
    let mut buffer = String::new();
    let mut handle = io::stdin();

    handle.read_to_string(&mut buffer)?;

    let memory: io::Result<Vec<usize>> =
        buffer
            .split_whitespace()
            .try_fold(Vec::new(), |mut acc, word| {
                let count = word.parse::<usize>().map_err(|_| {
                    io::Error::new(io::ErrorKind::Other, format!("{} is not a digit!", word))
                })?;

                acc.push(count);

                Ok(acc)
            });
    let mut memory = memory?;
    let mut history = Vec::new();

    while !history.contains(&memory) {
        history.push(memory.clone());
        cycle(&mut memory)?;
    }

    println!("{}", history.len() - find(&history, &memory)?);

    Ok(())
}

fn find<T>(vec: &[T], elem: &T) -> io::Result<usize>
where
    T: PartialEq,
{
    vec.iter()
        .enumerate()
        .find(|&(_, x)| x == elem)
        .map(|(index, _)| index)
        .ok_or_else(|| io::Error::new(io::ErrorKind::Other, "Element not found in vector"))
}

fn find_max_index(memory: &Vec<usize>) -> io::Result<usize> {
    let (_, index) = memory.iter().enumerate().fold((0, None), |acc, x| {
        let (max, max_index) = acc;
        let (index, value) = x;
        if value > &max {
            (*value, Some(index))
        } else {
            (max, max_index)
        }
    });

    index.ok_or_else(|| io::Error::new(io::ErrorKind::Other, "Memory vector is empty"))
}

fn cycle(memory: &mut Vec<usize>) -> io::Result<()> {
    let length = memory.len();

    let range = {
        let max_index = find_max_index(memory)?;
        let count = memory[max_index];
        memory[max_index] = 0;

        max_index + 1..max_index + count + 1
    };

    for index in range {
        let index = index % length;
        memory[index] += 1;
    }

    Ok(())
}
