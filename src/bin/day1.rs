use std::io::{self, Read};

fn main() -> io::Result<()> {
    let mut buffer = String::new();
    let handle = io::stdin();

    handle.lock().read_to_string(&mut buffer)?;

    let (result, first, previous) = buffer.trim().chars().try_fold((0, None, None), per)?;

    let result = match (first, previous) {
        (Some(first_value), Some(previous_value)) => {
            if first_value == previous_value {
                result + previous_value
            } else {
                result
            }
        }
        _ => result,
    };

    println!("{}", result);

    Ok(())
}

fn per(
    acc: (u32, Option<u32>, Option<u32>),
    value: char,
) -> io::Result<(u32, Option<u32>, Option<u32>)> {
    let (result, first, previous) = acc;
    let value = value
        .to_digit(10)
        .ok_or_else(|| io::Error::new(io::ErrorKind::Other, format!("Not a digit: {}", value)))?;

    let new_first = first.unwrap_or(value);
    let new_previous = Some(value);
    let new_result = previous.map_or(result, |prev| {
        if prev == value {
            result + value
        } else {
            result
        }
    });

    Ok((new_result, Some(new_first), new_previous))
}
