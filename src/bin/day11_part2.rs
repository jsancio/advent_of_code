use anyhow::anyhow;
use anyhow::Error;
use anyhow::Result;
use core::cmp::min;
use std::io::Read;
use std::str::FromStr;

#[derive(Debug, Clone)]
struct HexGrid {
    ne: u32,
    se: u32,
    s: u32,
    sw: u32,
    nw: u32,
    n: u32,
}

impl HexGrid {
    fn new() -> Self {
        Self {
            ne: 0,
            se: 0,
            s: 0,
            sw: 0,
            nw: 0,
            n: 0,
        }
    }

    fn steps(&self) -> u64 {
        let mut steps = 0u64;
        steps += u64::from(self.ne);
        steps += u64::from(self.se);
        steps += u64::from(self.s);
        steps += u64::from(self.sw);
        steps += u64::from(self.nw);
        steps += u64::from(self.n);

        steps
    }

    fn update(&mut self, coordinate: &str) -> Result<()> {
        match coordinate {
            "ne" => self.ne += 1,
            "se" => self.se += 1,
            "s" => self.s += 1,
            "sw" => self.sw += 1,
            "nw" => self.nw += 1,
            "n" => self.n += 1,
            unknown => return Err(anyhow!("Unable to parse coordinate '{}'", unknown)),
        }

        Ok(())
    }

    fn reduce(&mut self) {
        while self.reduce_inverse() | self.reduce_alternate() {}
    }

    fn reduce_inverse(&mut self) -> bool {
        let mut reduced = false;

        let smallest = min(self.ne, self.sw);
        if smallest > 0 {
            reduced = true;
        }
        self.ne -= smallest;
        self.sw -= smallest;

        let smallest = min(self.se, self.nw);
        if smallest > 0 {
            reduced = true;
        }
        self.se -= smallest;
        self.nw -= smallest;

        let smallest = min(self.s, self.n);
        if smallest > 0 {
            reduced = true;
        }
        self.s -= smallest;
        self.n -= smallest;

        reduced
    }

    fn reduce_alternate(&mut self) -> bool {
        let mut reduced = false;

        let smallest = min(self.ne, self.s);
        if smallest > 0 {
            reduced = true;
        }
        self.ne -= smallest;
        self.s -= smallest;
        self.se += smallest;

        let smallest = min(self.se, self.sw);
        if smallest > 0 {
            reduced = true;
        }
        self.se -= smallest;
        self.sw -= smallest;
        self.s += smallest;

        let smallest = min(self.s, self.nw);
        if smallest > 0 {
            reduced = true;
        }
        self.s -= smallest;
        self.nw -= smallest;
        self.sw += smallest;

        let smallest = min(self.sw, self.n);
        if smallest > 0 {
            reduced = true;
        }
        self.sw -= smallest;
        self.n -= smallest;
        self.nw += smallest;

        let smallest = min(self.nw, self.ne);
        if smallest > 0 {
            reduced = true;
        }
        self.nw -= smallest;
        self.ne -= smallest;
        self.n += smallest;

        let smallest = min(self.n, self.se);
        if smallest > 0 {
            reduced = true;
        }
        self.n -= smallest;
        self.se -= smallest;
        self.ne += smallest;

        reduced
    }
}

impl FromStr for HexGrid {
    type Err = Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut grid = Self::new();
        for coordinate in s.split(',') {
            grid.update(coordinate)?;
        }

        Ok(grid)
    }
}

fn main() -> Result<()> {
    let mut stdin = std::io::stdin();

    let mut string = String::new();
    stdin.read_to_string(&mut string)?;

    let mut max_steps = 0;
    let mut grid = HexGrid::new();
    for coordinate in string.trim().split(',') {
        grid.update(coordinate)?;
        let mut partial = grid.clone();
        partial.reduce();

        let steps = partial.steps();
        if steps > max_steps {
            max_steps = steps;
        }
    }

    println!("{}", max_steps);

    Ok(())
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_inverse() {
        let mut grid: HexGrid = "ne,ne,sw,sw,se,se,nw,nw,s,s,n,n".parse().unwrap();
        grid.reduce();

        assert_eq!(grid.ne, 0);
        assert_eq!(grid.se, 0);
        assert_eq!(grid.s, 0);
        assert_eq!(grid.sw, 0);
        assert_eq!(grid.nw, 0);
        assert_eq!(grid.n, 0);
    }

    #[test]
    fn test_alternate() {
        let mut grid: HexGrid = "ne,ne,s,s,s".parse().unwrap();
        grid.reduce();
        assert_eq!(grid.se, 2);
        assert_eq!(grid.s, 1);

        let mut grid: HexGrid = "sw,sw,se,se,se".parse().unwrap();
        grid.reduce();
        assert_eq!(grid.s, 2);
        assert_eq!(grid.se, 1);

        let mut grid: HexGrid = "nw,nw,nw,s,s".parse().unwrap();
        grid.reduce();
        assert_eq!(grid.sw, 2);
        assert_eq!(grid.nw, 1);

        let mut grid: HexGrid = "n,n,n,sw,sw".parse().unwrap();
        grid.reduce();
        assert_eq!(grid.nw, 2);
        assert_eq!(grid.n, 1);

        let mut grid: HexGrid = "nw,nw,ne,ne,ne".parse().unwrap();
        grid.reduce();
        assert_eq!(grid.n, 2);
        assert_eq!(grid.ne, 1);

        let mut grid: HexGrid = "n,n,se,se,se".parse().unwrap();
        grid.reduce();
        assert_eq!(grid.ne, 2);
        assert_eq!(grid.se, 1);
    }
}
