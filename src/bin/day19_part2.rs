use anyhow::Result;
use std::collections::HashMap;
use std::io::Read;

#[derive(Debug, Copy, Clone)]
enum Place {
    Unnamed,
    Named(u8),
}

#[derive(Debug, Copy, Clone, PartialEq)]
enum Direction {
    Down,
    Up,
    Left,
    Right,
}

impl Direction {
    fn change_position(&self, x: u8, y: u8) -> (u8, u8) {
        match self {
            Self::Down => (x, y + 1),
            Self::Up => (x, y - 1),
            Self::Left => (x - 1, y),
            Self::Right => (x + 1, y),
        }
    }

    fn turns(&self) -> (Self, Self) {
        match self {
            Self::Down | Self::Up => (Self::Left, Self::Right),
            Self::Left | Self::Right => (Self::Up, Self::Down),
        }
    }
}

#[derive(Debug)]
struct Maze {
    map: HashMap<(u8, u8), Place>,
    x: u8,
    y: u8,
    direction: Direction,
}

impl Maze {
    fn new(map: HashMap<(u8, u8), Place>, x: u8, y: u8) -> Self {
        Self {
            map,
            x,
            y,
            direction: Direction::Down,
        }
    }

    fn advance(&mut self) -> Option<Place> {
        let (first_turn, second_turn) = self.direction.turns();

        let directions = [self.direction, first_turn, second_turn];

        if let Some(((new_x, new_y), new_direction, new_place)) =
            directions.iter().find_map(|direction| {
                let new_position = direction.change_position(self.x, self.y);

                self.map
                    .get(&new_position)
                    .map(|place| (new_position, *direction, *place))
            })
        {
            self.x = new_x;
            self.y = new_y;
            self.direction = new_direction;

            Some(new_place)
        } else {
            None
        }
    }

    fn walk(&mut self) -> (usize, String) {
        let mut result = String::new();
        let mut count = 1;

        while let Some(place) = self.advance() {
            if let Place::Named(id) = place {
                result.push(char::from(id + b'A'));
            }

            count += 1;
        }

        (count, result)
    }
}

fn main() -> Result<()> {
    let stdin = std::io::stdin();

    let mut map = HashMap::new();
    let mut y = 0;
    let mut x = 0;
    let mut start_x = 0;
    let mut reader = stdin.lock().bytes();
    loop {
        if let Some(byte) = reader.next() {
            let byte = byte?;
            match byte {
                b'\n' => {
                    y += 1;
                    x = 0;
                }
                b'|' | b'-' | b'+' => {
                    if map.is_empty() {
                        start_x = x;
                    }
                    map.insert((x, y), Place::Unnamed);
                    x += 1;
                }
                b'A'..=b'Z' => {
                    map.insert((x, y), Place::Named(byte - b'A'));
                    x += 1;
                }
                _ => {
                    x += 1;
                }
            }
        } else {
            break;
        }
    }

    let mut maze = Maze::new(map, start_x, 0);

    let (count, _) = maze.walk();

    println!("{}", count);

    Ok(())
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_change_down_position() {
        let position = Direction::Down.change_position(0, 0);

        assert_eq!(position, (0, 1));
    }

    #[test]
    fn test_change_up_position() {
        let position = Direction::Up.change_position(0, 1);

        assert_eq!(position, (0, 0));
    }

    #[test]
    fn test_change_right_position() {
        let position = Direction::Right.change_position(0, 0);

        assert_eq!(position, (1, 0));
    }

    #[test]
    fn test_change_left_position() {
        let position = Direction::Left.change_position(1, 0);

        assert_eq!(position, (0, 0));
    }

    #[test]
    fn test_up_turns() {
        let (first, second) = Direction::Up.turns();

        assert!(first != second);
        assert!(first == Direction::Left || second == Direction::Left);
        assert!(first == Direction::Right || second == Direction::Right);
    }

    #[test]
    fn test_down_turns() {
        let (first, second) = Direction::Down.turns();

        assert!(first != second);
        assert!(first == Direction::Left || second == Direction::Left);
        assert!(first == Direction::Right || second == Direction::Right);
    }

    #[test]
    fn test_left_turns() {
        let (first, second) = Direction::Left.turns();

        assert!(first != second);
        assert!(first == Direction::Down || second == Direction::Down);
        assert!(first == Direction::Up || second == Direction::Up);
    }

    #[test]
    fn test_right_turns() {
        let (first, second) = Direction::Right.turns();

        assert!(first != second);
        assert!(first == Direction::Down || second == Direction::Down);
        assert!(first == Direction::Up || second == Direction::Up);
    }
}
