use std::io::{self, BufRead, Result};

fn main() -> io::Result<()> {
    let handle = io::stdin();

    let checksum: Result<u32> = handle.lock().lines().try_fold(0, |acc, line| {
        let min_max = line?.split_whitespace().fold((None, None), |acc, x| {
            let (min, max) = acc;
            let x = x.parse::<u32>().unwrap();

            let min = min.map_or(x, |min| if x < min { x } else { min });

            let max = max.map_or(x, |max| if x > max { x } else { max });

            (Some(min), Some(max))
        });

        let diff = match min_max {
            (Some(min), Some(max)) => max - min,
            _ => 0,
        };

        Ok(acc + diff)
    });

    println!("{}", checksum?);

    Ok(())
}
