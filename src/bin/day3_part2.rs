use std::collections::HashMap;
use std::io::{self, Read};

#[derive(Copy, Clone, Debug)]
enum Direction {
    Right,
    Up,
    Left,
    Down,
}

impl Direction {
    fn next(self) -> Direction {
        match self {
            Direction::Up => Direction::Left,
            Direction::Left => Direction::Down,
            Direction::Down => Direction::Right,
            Direction::Right => Direction::Up,
        }
    }

    fn change(self, dist: (i32, i32)) -> (i32, i32) {
        let (vertical, horizontal) = dist;
        match self {
            Direction::Up => (vertical + 1, horizontal),
            Direction::Left => (vertical, horizontal - 1),
            Direction::Down => (vertical - 1, horizontal),
            Direction::Right => (vertical, horizontal + 1),
        }
    }
}

#[derive(Debug)]
struct SpiralLocation {
    direction: Direction,
    distance: (i32, i32),
    movement: u32,
    first_span: u32,
    second_span: u32,
}

impl SpiralLocation {
    fn new() -> SpiralLocation {
        SpiralLocation {
            direction: Direction::Right,
            distance: (0, 0),
            movement: 0,
            first_span: 1,
            second_span: 0,
        }
    }

    fn changes_direction(&self) -> bool {
        self.movement + 1 == self.first_span
    }

    fn next(&self) -> SpiralLocation {
        SpiralLocation {
            direction: if self.changes_direction() {
                self.direction.next()
            } else {
                self.direction
            },
            distance: self.direction.change(self.distance),
            movement: if self.changes_direction() {
                0
            } else {
                self.movement + 1
            },
            first_span: if self.changes_direction() && self.first_span == self.second_span {
                self.first_span + 1
            } else {
                self.first_span
            },
            second_span: if self.changes_direction() {
                self.first_span
            } else {
                self.second_span
            },
        }
    }
}

fn cell_value(cells: &HashMap<(i32, i32), u32>, position: (i32, i32)) -> u32 {
    let (vertical, horizontal) = position;
    let up = cells.get(&(vertical + 1, horizontal)).map_or(0, |x| *x);
    let up_left = cells.get(&(vertical + 1, horizontal - 1)).map_or(0, |x| *x);
    let left = cells.get(&(vertical, horizontal - 1)).map_or(0, |x| *x);
    let down_left = cells.get(&(vertical - 1, horizontal - 1)).map_or(0, |x| *x);
    let down = cells.get(&(vertical - 1, horizontal)).map_or(0, |x| *x);
    let down_right = cells.get(&(vertical - 1, horizontal + 1)).map_or(0, |x| *x);
    let right = cells.get(&(vertical, horizontal + 1)).map_or(0, |x| *x);
    let up_right = cells.get(&(vertical + 1, horizontal + 1)).map_or(0, |x| *x);

    up + up_left + left + down_left + down + down_right + right + up_right
}

fn main() -> io::Result<()> {
    let mut buffer = String::new();
    let mut cells = HashMap::new();
    let handle = io::stdin();

    handle.lock().read_to_string(&mut buffer)?;

    let input = buffer
        .trim()
        .parse::<u32>()
        .map_err(|_| io::Error::new(io::ErrorKind::Other, format!("{} is not a digit!", buffer)))?;

    cells.insert((0, 0), 1);
    let mut result = 0;
    let mut acc = SpiralLocation::new();
    while result <= input {
        acc = acc.next();

        let value = cell_value(&cells, acc.distance);
        cells.insert(acc.distance, value);

        result = value;
    }

    println!("{}", result);

    Ok(())
}
