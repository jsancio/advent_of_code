use anyhow::Result;

struct Generator {
    prev: u64,
    factor: u64,
}

impl Generator {
    fn new(initial: u64, factor: u64) -> Self {
        Self {
            prev: initial,
            factor,
        }
    }
}

impl Iterator for Generator {
    type Item = u64;

    fn next(&mut self) -> Option<Self::Item> {
        const MODULUS: u64 = 2147483647;
        self.prev = (self.prev * self.factor) % MODULUS;

        Some(self.prev)
    }
}

fn match_lowest_bits(a: u64, b: u64) -> bool {
    const MASK: u64 = 0xffff;
    let a_lowest = a & MASK;
    let b_lowest = b & MASK;

    a_lowest == b_lowest
}

fn count_lowest_bit_matches<A, B>(gen_a: A, gen_b: B) -> usize
where
    A: Iterator<Item = u64>,
    B: Iterator<Item = u64>,
{
    const TAKE: usize = 40_000_000;

    let mut count = 0;
    for (a, b) in gen_a.zip(gen_b).take(TAKE) {
        if match_lowest_bits(a, b) {
            count += 1;
        }
    }

    count
}

fn main() -> Result<()> {
    let gen_a = Generator::new(679, 16807);
    let gen_b = Generator::new(771, 48271);

    println!("{}", count_lowest_bit_matches(gen_a, gen_b));

    Ok(())
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_generator() {
        let gen = Generator::new(65, 16807);
        let expected = [1092455, 1181022009, 245556042, 1744312007, 1352636452];

        for (g, e) in gen.zip(expected.iter()).take(5) {
            assert_eq!(g, *e);
        }
    }

    #[test]
    fn test_match_lowest_bits() {
        let a = 245556042;
        let b = 1431495498;

        assert!(match_lowest_bits(a, b));
    }

    #[test]
    fn test_count_lowest_bit_match() {
        let gen_a = Generator::new(65, 16807);
        let gen_b = Generator::new(8921, 48271);

        assert_eq!(count_lowest_bit_matches(gen_a, gen_b), 588);
    }
}
