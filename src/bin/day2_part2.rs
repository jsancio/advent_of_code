use std::io::{self, BufRead, Result};

fn main() -> io::Result<()> {
    let handle = io::stdin();

    let checksum: Result<u32> = handle.lock().lines().try_fold(0, |acc, line| {
        let line = line?;
        let numbers: Vec<&str> = line.split_whitespace().collect();

        numbers.iter().enumerate().try_fold(acc, |acc, x| {
            let (index, item) = x;
            let first = item.parse::<u32>().map_err(|_| {
                io::Error::new(io::ErrorKind::Other, format!("{} is not a digit!", item))
            })?;

            numbers.iter().skip(index + 1).try_fold(acc, |acc, x| {
                let second = x.parse::<u32>().map_err(|_| {
                    io::Error::new(io::ErrorKind::Other, format!("{} is not a digit!", x))
                })?;

                let division = if first >= second && first % second == 0 {
                    first / second
                } else if second >= first && second % first == 0 {
                    second / first
                } else {
                    0
                };

                Ok(acc + division)
            })
        })
    });

    println!("{}", checksum?);

    Ok(())
}
