use anyhow::anyhow;
use anyhow::Result;
use std::io::Read;

enum Parser {
    Start,
    End(u32),
    Group(Group),
    Garbage(Garbage),
}

impl Parser {
    fn new() -> Self {
        Parser::Start
    }

    fn parse(self, c: u8) -> Result<Parser> {
        match self {
            Parser::Start => match c {
                b'{' => Ok(Parser::Group(Group::new())),
                _ => Err(anyhow!(
                    "Invalid character, expected '{}' and found '{}' ({})",
                    '{',
                    char::from(c),
                    c
                )),
            },
            Parser::End(_) => Err(anyhow!(
                "Invalid character in End state: '{}' ({})",
                char::from(c),
                c
            )),
            Parser::Group(group) => group.parse(c),
            Parser::Garbage(garbage) => garbage.parse(c),
        }
    }

    fn score(&self) -> Result<u32> {
        match self {
            Parser::Start => Err(anyhow!("Invalid score while outside a group")),
            Parser::End(score) => Ok(*score),
            Parser::Group(group) => {
                Err(anyhow!("Invalid score while processing group: {:?}", group))
            }
            Parser::Garbage(garbage) => Err(anyhow!(
                "Invalid score while processing garbage: {:?}",
                garbage
            )),
        }
    }
}

#[derive(Debug)]
struct Group {
    score: u32,
    group_level: u32,
}

impl Group {
    fn new() -> Self {
        Group {
            score: 1,
            group_level: 1,
        }
    }

    fn parse(mut self, c: u8) -> Result<Parser> {
        match c {
            b'{' => {
                self.group_level += 1;
                self.score += self.group_level;
                Ok(Parser::Group(self))
            }
            b'}' => {
                self.group_level -= 1;
                if self.group_level == 0 {
                    Ok(Parser::End(self.score))
                } else {
                    Ok(Parser::Group(self))
                }
            }
            b'<' => Ok(Parser::Garbage(Garbage::new(self))),
            b',' => {
                // ignored within a group
                Ok(Parser::Group(self))
            }
            _ => Err(anyhow!(
                "Invalid character in a group '{}' ({})",
                char::from(c),
                c
            )),
        }
    }
}

#[derive(Debug)]
struct Garbage {
    group: Group,
    ignore_char: bool,
}

impl Garbage {
    fn new(group: Group) -> Self {
        Garbage {
            group,
            ignore_char: false,
        }
    }

    fn parse(mut self, c: u8) -> Result<Parser> {
        if self.ignore_char {
            self.ignore_char = false;
            Ok(Parser::Garbage(self))
        } else {
            match c {
                b'>' => Ok(Parser::Group(self.group)),
                b'!' => {
                    self.ignore_char = true;
                    Ok(Parser::Garbage(self))
                }
                _ => Ok(Parser::Garbage(self)),
            }
        }
    }
}

fn main() -> Result<()> {
    let stdin = std::io::stdin();

    let score = score(
        stdin
            .lock()
            .bytes()
            .inspect(|c| {
                if let Err(e) = c {
                    println!("Error reading from stdin: {}", e);
                }
            })
            .filter_map(|r| r.ok().filter(|u| *u != b'\n')),
    )?;

    println!("{}", score);

    Ok(())
}

fn score<T>(input: T) -> Result<u32>
where
    T: Iterator<Item = u8>,
{
    let mut parser = Parser::new();
    for c in input {
        parser = parser.parse(c)?;
    }

    parser.score()
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_basic_groups() {
        assert_eq!(test_score("{}"), 1);
        assert_eq!(test_score("{{{}}}"), 6);
        assert_eq!(test_score("{{},{}}"), 5);
        assert_eq!(test_score("{{{},{},{{}}}}"), 16);
    }

    #[test]
    fn test_basic_garbage() {
        assert_eq!(test_score("{<a>,<a>,<a>,<a>}"), 1);
        assert_eq!(test_score("{{<ab>},{<ab>},{<ab>},{<ab>}}"), 9);
        assert_eq!(test_score("{{<!!>},{<!!>},{<!!>},{<!!>}}"), 9);
        assert_eq!(test_score("{{<a!>},{<a!>},{<a!>},{<ab>}}"), 3);
    }

    fn test_score(input: &str) -> u32 {
        score(input.as_bytes().iter().map(|u| *u)).unwrap()
    }
}
