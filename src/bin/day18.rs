use anyhow::anyhow;
use anyhow::Result;
use nom::branch::alt;
use nom::bytes::complete::tag;
use nom::character::complete::digit1;
use nom::combinator::map;
use nom::combinator::map_res;
use nom::combinator::opt;
use nom::error::make_error;
use nom::error::ErrorKind;
use nom::IResult;
use std::convert::TryFrom;
use std::convert::TryInto;
use std::io::Read;
use std::str::FromStr;

#[derive(Debug)]
enum Value {
    Number(i64),
    Register(u8),
}

#[derive(Debug)]
enum Operation {
    Add(u8, Value),
    Mod(u8, Value),
    Mul(u8, Value),
    Set(u8, Value),
    Jgz(Value, Value),
    Rcv(u8),
    Snd(u8),
}

impl Operation {
    fn execute(&self, context: &mut Context) {
        let mut skip = match self {
            Operation::Add(register, value) => {
                *context.register_mut(*register) += context.number(value);
                1
            }
            Operation::Mod(register, value) => {
                *context.register_mut(*register) %= context.number(value);
                1
            }
            Operation::Mul(register, value) => {
                *context.register_mut(*register) *= context.number(value);
                1
            }
            Operation::Set(register, value) => {
                *context.register_mut(*register) = context.number(value);
                1
            }
            Operation::Jgz(first, second) => {
                if context.number(first) > 0 {
                    context.number(second)
                } else {
                    1
                }
            }
            Operation::Rcv(register) => {
                if context.register(*register) != 0 {
                    println!("{}", context.played);
                    100
                } else {
                    1
                }
            }
            Operation::Snd(register) => {
                context.played = context.register(*register);
                1
            }
        };

        if skip > 0 {
            context.instruction_counter += usize::try_from(skip).unwrap();
        } else {
            skip *= -1;
            context.instruction_counter -= usize::try_from(skip).unwrap();
        }
    }
}

struct Context {
    registers: Vec<i64>,
    instruction_counter: usize,
    played: i64,
}

impl Context {
    fn new() -> Self {
        Self {
            registers: vec![0; 16],
            instruction_counter: 0,
            played: 0,
        }
    }
    fn number(&self, value: &Value) -> i64 {
        match value {
            &Value::Number(number) => number,
            &Value::Register(register) => self.register(register),
        }
    }

    fn register_mut(&mut self, register: u8) -> &mut i64 {
        &mut self.registers[usize::from(register)]
    }

    fn register(&self, register: u8) -> i64 {
        self.registers[usize::from(register)]
    }
}

fn number(input: &str) -> IResult<&str, i64> {
    let (input, sign) = opt(tag("-"))(input)?;
    let (input, number) = map_res(digit1, i64::from_str)(input)?;

    let sign = sign.map_or(1, |_| -1);

    Ok((input, sign * number))
}

fn register(input: &str) -> IResult<&str, u8> {
    if let Some(c) = input.chars().next() {
        if c.is_ascii_alphabetic() {
            let input = &input[1..];
            // SAFETY: unwrap is safe because is_ascii_alphabetic is true
            let mut register = u32::from(c).try_into().unwrap();
            register -= b'a';
            Ok((input, register))
        } else {
            Err(nom::Err::Error(make_error(input, ErrorKind::Char)))
        }
    } else {
        Err(nom::Err::Error(make_error(input, ErrorKind::Eof)))
    }
}

fn value(input: &str) -> IResult<&str, Value> {
    let number_value = map(number, |n| Value::Number(n));
    let register_value = map(register, |r| Value::Register(r));

    alt((number_value, register_value))(input)
}

fn resgister_and_value(input: &str) -> IResult<&str, (u8, Value)> {
    let (input, register) = register(input)?;
    let (input, _) = tag(" ")(input)?;
    let (input, value) = value(input)?;

    Ok((input, (register, value)))
}

fn add(input: &str) -> IResult<&str, Operation> {
    let (input, _) = tag("add ")(input)?;
    let (input, (register, value)) = resgister_and_value(input)?;

    Ok((input, Operation::Add(register, value)))
}

fn jgz(input: &str) -> IResult<&str, Operation> {
    let (input, _) = tag("jgz ")(input)?;
    let (input, first) = value(input)?;
    let (input, _) = tag(" ")(input)?;
    let (input, second) = value(input)?;

    Ok((input, Operation::Jgz(first, second)))
}

fn modulo(input: &str) -> IResult<&str, Operation> {
    let (input, _) = tag("mod ")(input)?;
    let (input, (register, value)) = resgister_and_value(input)?;

    Ok((input, Operation::Mod(register, value)))
}

fn mul(input: &str) -> IResult<&str, Operation> {
    let (input, _) = tag("mul ")(input)?;
    let (input, (register, value)) = resgister_and_value(input)?;

    Ok((input, Operation::Mul(register, value)))
}

fn set(input: &str) -> IResult<&str, Operation> {
    let (input, _) = tag("set ")(input)?;
    let (input, (register, value)) = resgister_and_value(input)?;

    Ok((input, Operation::Set(register, value)))
}

fn rcv(input: &str) -> IResult<&str, Operation> {
    let (input, _) = tag("rcv ")(input)?;
    let (input, register) = register(input)?;

    Ok((input, Operation::Rcv(register)))
}

fn snd(input: &str) -> IResult<&str, Operation> {
    let (input, _) = tag("snd ")(input)?;
    let (input, register) = register(input)?;

    Ok((input, Operation::Snd(register)))
}

fn operation(input: &str) -> IResult<&str, Operation> {
    alt((add, jgz, modulo, mul, set, rcv, snd))(input)
}

fn main() -> Result<()> {
    let mut stdin = std::io::stdin();

    let mut content = String::new();

    stdin.read_to_string(&mut content)?;

    let operations: Vec<Operation> = content
        .lines()
        .map(|line| {
            operation(line)
                .map(|(_, ops)| ops)
                .map_err(|error| anyhow!("Error parsing: {:?}", error))
        })
        .collect::<Result<_, _>>()?;

    let mut context = Context::new();
    loop {
        let ops = &operations[context.instruction_counter];
        ops.execute(&mut context);

        if context.instruction_counter >= operations.len() {
            break Ok(());
        }
    }
}
